from hashlib import new
from flask import Flask, request, redirect
from flask import jsonify
from flask.templating import render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, true
from flask_migrate import Migrate, migrate
from werkzeug.security import generate_password_hash, check_password_hash
import re
from uuid import uuid4


# needs refining (search can be vastly improved, login tokens can be added )
app=Flask(__name__)
app.debug = True

app.config['Secret_key']="newkey"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///contacts.db'

db=SQLAlchemy(app)

class user(db.Model):
    name=db.Column(db.String(50) ,nullable=False)
    id=db.Column(db.Integer ,nullable=False , primary_key=True)
    address=db.Column(db.String(50) ,nullable=False)
    number = db.Column(db.Integer, nullable=False)
    email =db.Column(db.String(50) , nullable=False)
    password = db.Column(db.String(80), nullable=False)

   
class contact(db.Model):
    c_name=db.Column(db.String(50) ,nullable=False)
    c_number=db.Column(db.Integer)
    c_email=db.Column(db.String(50))
    c_addressd=db.Column(db.String(80))
    c_country=db.Column(db.String(50))
    c_id=db.Column(db.Integer ,nullable=False , primary_key=True)
    u_id=db.Column(db.Integer ,nullable=False )
    

#fetch user data
@app.route('/user/<user_id>',methods=['GET'])
def user_data(user_id):
    data=user.query.filter_by(id=user_id).first()
    return jsonify({'name':data.name, 'email':data.email, 'address':data.address, 'number':data.number})          

#add new contact
@app.route('/<user_id>/contacts',methods=['POST'])
def add_data(user_id):
    data = request.get_json() 
  #  hashed_password = generate_password_hash(data['password'],method='sha256')
    new_contact = contact(c_name=data['name'], c_email=data['email'], c_addressd=data['address'], c_number=data['number'],c_country=data['country'] ,u_id=user_id)
    db.session.add(new_contact)
    db.session.commit()
    return jsonify({'new_user': new_contact.c_id})

#list all contacts
@app.route('/<user_id>/contacts/all',methods=['GET'])
def contact_list(user_id):
    saved_data=contact.query.filter_by(u_id=user_id)

    contacts=[]
    for data in saved_data:
        user_data={}
        user_data['name']=data.c_name
        user_data['email']=data.c_email
        user_data['address']=data.c_addressd
        user_data['country']=data.c_country
        user_data['number']=data.c_number
        contacts.append(user_data)
    
    return jsonify(contacts) 

#signup
@app.route('/user',methods=['POST'])
def signup():
    data = request.get_json() 
    hashed_password = generate_password_hash(data['password'],method='sha256')
    new_user = user(name=data['name'], email=data['email'], password=hashed_password, address=data['address'], number=data['number']) 
    db.session.add(new_user)
    db.session.commit()
    return jsonify({'new_user': new_user.id})


#login 
@app.route('/user/login', methods=['POST'])
def login():
    data =request.get_json()
 #   compare hash for validation
    current_user=user.query.filter_by(email=data['email']).first()
    if check_password_hash(current_user.password ,data['password']):
        return jsonify({'user': "valid"})
    else:
        return jsonify({'user':current_user.password})

#search            #so far search only one name 
@app.route('/user/search', methods=['GET'])
def search():
    saved_data=user.query.all()
    temp=request.get_json()
    users=[]
    for data in saved_data:
        user_data={}
        user_data['name']=data.name
        user_data['email']=data.email
        user_data['address']=data.address
        user_data['number']=data.number
        if temp['name']==data.name or temp['name']==data.email or temp['name']==data.number:
      #  user_data['country']=data.c_country
      #  user_data['number']=data.c_number
            users.append(user_data)
    
    return jsonify(users) 
            

if __name__=='__main__':
    app.run(debug=True)
